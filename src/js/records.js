import { pangram } from './pangram';

export const records = (() => {
  const recordsEl = document.querySelector('#records');
  const recordTemplate = recordsEl.querySelector('#record-template');

  function add(record) {
    recordsEl.insertBefore(createRecordTemplate(record), recordsEl.childNodes[0]);
  }

  // @param pangram.Pangram
  // Inserts pangram data to DOM node cloned from record template
  // @returns DOM node
  function createRecordTemplate(record) {
    const recordNode = document.importNode(recordTemplate.content, true).querySelector('.record');

    const status = !record.isPangram() ?
      `${pangram.alphabet.length - record.missingLetters().length}/${pangram.alphabet.length}`
      : '	\u2714';

    recordNode.innerHTML = recordNode.innerHTML
      .replace('{{value}}', record.value)
      .replace('{{status}}', status);

    return recordNode;
  }

  return {
    add,
  };
})();
