export const pangram = (() => {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz';

  function Pangram(value) {
    this.value = value;
  }

  // Returns all letters in alphabet that are not found in string
  Pangram.prototype.missingLetters = function() {
    // remove non alphabet chars from string
    const alphabetOnly = this.value.replace(/[^a-z]/gi, '').toLowerCase();

    return alphabet.split('')
      .filter((letter) => alphabetOnly.indexOf(letter) === -1);
  };

  Pangram.prototype.isPangram = function() {
    return this.missingLetters().length === 0;
  };

  return {
    Pangram,
    alphabet,
  };
})();
