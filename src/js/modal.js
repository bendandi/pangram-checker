
export const modal = (() => {
  const verifyModalEl = document.querySelector('#modal');
  const closeModalEl = verifyModalEl.querySelector('#close-modal');

  closeModalEl.addEventListener('click', close);

  function open(input) {
    verifyModalEl.style.display = 'block';
    runVerifyAnimation(input);
  }

  function close() {
    verifyModalEl.style.display = 'none';
    verifyModalEl.querySelector('.modal-header').textContent = '';
    verifyModalEl.querySelector('.modal-header').classList.remove('text-warning');
    verifyModalEl.querySelectorAll('.modal-grid-letter--muted')
      .forEach((el) => el.classList.remove('modal-grid-letter--muted'));
  }

  // @param pangram.Pangram
  // runs an animated check on pangram in modal
  function runVerifyAnimation(input) {
    input.value.split('').forEach((char, index) => {
      // type out pangram
      setTimeout(function() {
        const valueEl = verifyModalEl.querySelector('.modal-header');
        valueEl.textContent += char;

        // if a letter of the alphabet is used, add class to style the letter as used
        if (/^[A-z]+$/.test(char)) {
          document.querySelector(`#${char.toLowerCase()}`).classList.add('modal-grid-letter--muted');
        }

        // when complete show whether pangram is valid or not
        if (index === input.value.length - 1) {
          if (input.isPangram()) {
            valueEl.textContent += '  \u2714';
          } else {
            valueEl.classList.add('text-warning');
            valueEl.textContent += '  \u2639';
          }
        }
      }, 400 * index);
    });
  }

  return {
    open,
  };
})();
