import { pangram } from './pangram';
import { modal } from './modal';
import { records } from './records';

(() => {
  const inputEl = document.querySelector('#pangram-input');
  const verifyButtonEl = document.querySelector('#verify-button');

  // Submit input on enter & button click
  inputEl.addEventListener('keypress', sumbitOnEnter);
  verifyButtonEl.addEventListener('click', submit);

  function sumbitOnEnter(e) {
    if (e.keyCode === 13) {
      e.preventDefault();
      submit();
    }
  }

  // Handle pangram submission
  function submit() {
    if (inputEl.textContent === '') {
      return;
    }

    const input = new pangram.Pangram(inputEl.textContent);

    modal.open(input);
    // modal.runVisualVerify(input);
    records.add(input);

    inputEl.textContent = '';
  }

  inputEl.focus();
})();
