const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const debug = process.env.NODE_ENV !== "production";

module.exports = {
  context: __dirname,
  devtool: debug ? "inline-sourcemap" : null,
  entry: ['./src/js/app.js', './src/sass/main.scss'],
  output: {
    //path: __dirname,
    filename: 'dist/bundle.min.js'
  },
  module: {
    rules: [
      { // sass / scss loader for webpack
        test: /\.(sass|scss)$/,
        loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
      },
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: "dist/styles.css",
      allChunks: true,
    }),
  ],
}
